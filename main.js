


//fait apparaitre les méchants par la droite, gauche et bas :


function slideFromLeft() {
	var elementArray = document.querySelectorAll('.fromLeft');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visible')
	}
}

document.querySelector('.slideButton').addEventListener('click', slideFromLeft, {once: true});


function slideFromLeftDeux() {
	var elementArray = document.querySelectorAll('.fromLeftDeux');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleDeux')
	}
}

document.querySelector('.slideButtonDeux').addEventListener('click', slideFromLeftDeux, {once: true});


function slideFromLeftTrois() {
	var elementArray = document.querySelectorAll('.fromLeftTrois');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleTrois')
	}
}

document.querySelector('.slideButtonTrois').addEventListener('click', slideFromLeftTrois, {once: true});


function slideFromLeftQuatre() {
	var elementArray = document.querySelectorAll('.fromLeftQuatre');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleQuatre')
	}
}

document.querySelector('.slideButtonQuatre').addEventListener('click', slideFromLeftQuatre, {once: true});


function slideFromLeftCinq() {
	var elementArray = document.querySelectorAll('.fromLeftCinq');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleCinq')
	}
}

document.querySelector('.slideButtonCinq').addEventListener('click', slideFromLeftCinq, {once: true});



//fait apparaitre gru après boutton mais surtout :

function Apparition() {
	var elementArray = document.querySelectorAll('.Apparition');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visible')
	}
}

document.querySelector('.ApparitionButton').addEventListener('click', Apparition, {once: true});



//fonctions qui affichent les mentions honorables

function ApparitionP11() {
	var elementArray = document.querySelectorAll('.ApparitionP11');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP11')
	}
}

function ApparitionP10() {
	var elementArray = document.querySelectorAll('.ApparitionP10');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP10')
	}
}

function ApparitionP9() {
	var elementArray = document.querySelectorAll('.ApparitionP9');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP9')
	}
}

function ApparitionP8() {
	var elementArray = document.querySelectorAll('.ApparitionP8');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP8')
	}
}

function ApparitionP7() {
	var elementArray = document.querySelectorAll('.ApparitionP7');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP7')
	}
}

function ApparitionP6() {
	var elementArray = document.querySelectorAll('.ApparitionP6');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP6')
	}
}

function ApparitionP5() {
	var elementArray = document.querySelectorAll('.ApparitionP5');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP5')
	}
}

function ApparitionP4() {
	var elementArray = document.querySelectorAll('.ApparitionP4');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP4')
	}
}

function ApparitionP3() {
	var elementArray = document.querySelectorAll('.ApparitionP3');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP3')
	}
}

function ApparitionP2() {
	var elementArray = document.querySelectorAll('.ApparitionP2');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP2')
	}
}

//Affiche les mentions honorables une par une avec 500ms 
//d'attente à chaque fois :

function ApparitionP() {
	var elementArray = document.querySelectorAll('.ApparitionP');
	for (var i = 0; i< elementArray.length; i++) {
		elementArray[i].classList.toggle('visibleP')
	}
	setTimeout(ApparitionP2, 500);
	setTimeout(ApparitionP3, 1000);
	setTimeout(ApparitionP4, 1500);
	setTimeout(ApparitionP5, 2000);
	setTimeout(ApparitionP6, 2500);
	setTimeout(ApparitionP7, 3000);
	setTimeout(ApparitionP8, 3500);
	setTimeout(ApparitionP9, 4000);
	setTimeout(ApparitionP10, 4500);
	setTimeout(ApparitionP11, 5000);
}

document.querySelector('.ApparitionButtonP').addEventListener('click', ApparitionP, {once: true});




//But de code avec lequel je voulais faire afficher toutes les 
//mentions honorables lorsque la div serait visible sur la fenêtre :
//
// $(window).scroll(function() {
//  	var positionsouris = $(window).scrollTop();
//  	var positiondiv = $("mentionsHonorable").position().top;
//  	if (positionsouris = positiondiv){
//  		ApparitionP();
//  	}
// });